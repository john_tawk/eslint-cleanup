# Code cleanup in ESLINT
Automatically cleans old syntax and fixing javascript standards.

> Note: Better to use the latest version of node when using this tool.

## Usage
`eslint -c .eslintrc.json --fix '<path>/**/*.js'`

example:

```$ eslint -c .eslintrc.json --fix /node-js-application/**/*.js```

> Refer to https://eslint.org/ for whats being fixed. Rules are in .eslintrc.json


